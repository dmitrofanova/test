var createError = require('http-errors');
var mysql = require('mysql');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
// var router = express.Router();

var favicon = require('serve-favicon');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var loginRouter = require('./routes/login');
var quotesRouter = require('./routes/quotes');
var admQuotesRouter = require('./routes/adm_quotes');
var delQuoteRouter = require('./routes/del_quote');
var editQuoteRouter = require('./routes/edit_quote');
var eaRouter = require('./routes/ea');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
// app.engine('html', require('ejs').renderFile);
// app.set('view engine', 'html');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/login', loginRouter);
app.use('/quotes', quotesRouter);
app.use('/adm_quotes', admQuotesRouter);
app.use('/del_quote', delQuoteRouter);
app.use('/edit_quote', editQuoteRouter);
app.use('/ea', eaRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});



app.use(favicon(path.join(__dirname,'public','images','favicon.ico')));
module.exports = app;

// app.listen(3000, function (){
//     console.log('listening on port', 3000);
// }
// );