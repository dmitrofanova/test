var express = require('express');
var mysql = require('mysql');
var router = express.Router();
var dateFormat = require('dateformat');

function uniqueArray0(array) {
  var result = Array.from(new Set(array));
  return result    
}

/* GET home page. */
router.get('/', function(req, res) {

	if(!req.cookies.jiracook){
		return res.render('login');
	}
	
	var quotesList = [];
	var typesList = [];
	var author = req.query.author;
	var type = req.query.type;
	var category = req.query.category;

	var pool = mysql.createPool({
	connectionLimit : 2,
    host: 'localhost',    
    port: 3307,
    user: 'root',
    password: 'aZx112',
    database: 'jirateam'
});

	if (author && author != ''){
		sql = 'SELECT q.id, q.text, q.img, q.created, q.description, a.name as author, q.author_id,a.icon, q.category_id, c.name as category, c.img as category_img, t.id as typeid, t.name as type, t.img as typeimg  FROM quotes q left join authors a on q.author_id=a.id left join categories c on q.category_id=c.id left join types t on q.type_id=t.id where author_id = ' + author + ' order by q.created';
	} else if (type && type != '') {
		sql = 'SELECT q.id, q.text, q.img, q.created, q.description, a.name as author, q.author_id,a.icon, q.category_id, c.name as category, c.img as category_img, t.id as typeid, t.name as type, t.img as typeimg  FROM quotes q left join authors a on q.author_id=a.id left join categories c on q.category_id=c.id left join types t on q.type_id=t.id where type_id = ' + type+ ' order by q.created';
	} else if (category && category != '') {
		sql = 'SELECT q.id, q.text, q.img, q.created, q.description, a.name as author, q.author_id,a.icon, q.category_id, c.name as category, c.img as category_img, t.id as typeid, t.name as type, t.img as typeimg  FROM quotes q left join authors a on q.author_id=a.id left join categories c on q.category_id=c.id left join types t on q.type_id=t.id where category_id = ' + category+ ' order by q.created';
	}
	 else {
		sql ='SELECT q.id, q.text, q.img, q.created, q.description, a.name as author, q.author_id,a.icon, q.category_id, c.name as category, c.img as category_img, t.id as typeid, t.name as type, t.img as typeimg  FROM quotes q left join authors a on q.author_id=a.id left join categories c on q.category_id=c.id left join types t on q.type_id=t.id order by q.created';
	}

	// sql1 = 'SELECT id, name, img from types';

console.log(sql);

pool.query(sql, function(err, rows, fields) {
		if (err) {
			res.status(500).json({"status_code": 500,"status_message": "internal server error"});
		} else {
			for (var i = 0; i < rows.length; i++) {

				var quotes = {
					'id':rows[i].id,
					'text':rows[i].text,
					'img':rows[i].img,
					'created':dateFormat(rows[i].created,"dd.mm.yyyy"),
					'description':rows[i].description,
					'author':rows[i].author,
					'author_id':rows[i].author_id,
					'icon':rows[i].icon,
					'category':rows[i].category,
					'category_id':rows[i].category_id,
					'type':rows[i].type,
					'type_id':rows[i].typeid,
					'category_img':rows[i].category_img
				}				
				quotesList.push(quotes);
		}

		var unique = {};
		var distinct = [];
		    for( var i in quotesList ){
		     if( typeof(unique[quotesList[i].type_id]) == "undefined"){
		      distinct.push({'id':quotesList[i].type_id, 'name':quotesList[i].type, 'img':quotesList[i].type_img});		      
		     }
		     unique[quotesList[i].type_id] = 0;
		    }
		// console.log(distinct);
		// console.log(distinct.length);
		res.render('quotes', {"quotesList": quotesList, "typesList": distinct, title: 'Quotes'});
		}
	});

});


module.exports = router;
