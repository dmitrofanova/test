var express = require('express');
var router = express.Router();

var cookieParser = require('cookie-parser');

var app = express();
app.use(cookieParser());


/* GET home page. */
router.get('/', function(req, res, next) {
	
	if(!req.cookies.jiracook){
		return res.redirect('/login');
	} else {
  		res.render('index', { title: 'Jira Team'});
  	}
});

router.get('/tst', function(req, res, next) {
	
	
  		res.render('test');
  	
});

module.exports = router;
