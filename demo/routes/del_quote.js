var express = require('express');
var mysql = require('mysql');
var router = express.Router();
var dateFormat = require('dateformat');


/* GET home page. */
router.get('/', function(req, res,next) {

	if(!req.cookies.jiracook){
		return res.render('login');
	}
	
	var quotesList = [];
	var id = req.query.id;

	var pool = mysql.createPool({
		connectionLimit : 2,
	    host: 'localhost',    
	    port: 3307,
	    user: 'root',
	    password: 'aZx112',
	    database: 'jirateam'
	});

	sql = 'SELECT q.id, q.text, q.img, q.created, q.description, a.name as author, q.author_id,a.icon, q.category_id, c.name as category, c.img as category_img, t.id as typeid, t.name as type, t.img as typeimg  FROM quotes q left join authors a on q.author_id=a.id left join categories c on q.category_id=c.id left join types t on q.type_id=t.id where q.id = ' + id ;	

		console.log(sql);

			pool.query(sql, function(err, rows, fields) {
					if (err) {
						res.status(500).json({"status_code": 500,"status_message": "internal server error"});						
					} else {
						console.log("rows: "+rows[0].text);
						for (var i = 0; i < rows.length; i++) {
							var quotes = {
								'id':rows[i].id,
								'text':rows[i].text,
								'img':rows[i].img,
								'created':dateFormat(rows[i].created,"dd.mm.yyyy"),
								'description':rows[i].description,					
								'author_id':rows[i].author_id,					
								'category_id':rows[i].category_id,					
								'type_id':rows[i].type_id,
								'author':rows[i].author,
								'category':rows[i].category,
								'type':rows[i].type
							}				
							quotesList.push(quotes);
						}
						res.render('del_quote', {"quotesList": quotesList, title: 'Del Quote'});
					}
			});
			console.log("sss " + quotesList.length);
			
			

});

router.post('/', function(req, res) {

		if(!req.cookies.jiracook){
			return res.render('login');
		}		

		var id = req.query.id;

		var pool = mysql.createPool({
		connectionLimit : 2,
	    host: 'localhost',    
	    port: 3307,
	    user: 'root',
	    password: 'aZx112',
	    database: 'jirateam'
	});
		
		var sql = "delete from quotes where id = " + id;
		// var sql ="fff";
		console.log(sql);

		pool.query(sql, function(err, rows, fields) {
				if (err) {
					res.status(500).json({"status_code": 500,"status_message": "internal server error"});					
				} else {
					res.redirect('/adm_quotes');				
				}
			});
		

});

module.exports = router;