var express = require('express');
var mysql = require('mysql');
var router = express.Router();



/* GET home page. */
router.get('/', function(req, res) {

	if(!req.cookies.jiracook){
		return res.render('login');
	}

	var usersList = [];
	var type = req.query.type;

	var pool = mysql.createPool({
	connectionLimit : 2,
    host: 'localhost',    
    port: 3307,
    user: 'root',
    password: 'aZx112',
    database: 'jirateam'
});

	// if (type && type != ''){
	// 	sql = 'SELECT a.id, a.name, a.description, a.icon, a.type, ach.name as ach_name, ach.img,(SELECT count(id) as qnum FROM quotes where author_id = a.id group by author_id) as num FROM authors a left join achivments ach on a.id =  ach.author_id where type = "' + type +'"';
	// } else {
	// 	sql ='SELECT a.id, a.name, a.description, a.icon, a.type, ach.name as ach_name, ach.img,(SELECT count(id) as qnum FROM quotes where author_id = a.id group by author_id) as num FROM authors a left join achivments ach on a.id =  ach.author_id order by a.type, a.name';
	// }
	if (type && type != ''){
		sql = 'SELECT a.id, a.name, a.description, a.icon, a.type, ach.name as ach_name, ach.img,if((SELECT count(id) as qnum FROM quotes where author_id = a.id group by author_id)=(select max(qnum) from (SELECT count(id) as qnum,author_id FROM quotes group by author_id) as tbl),1,NULL) as lead,(SELECT count(id) as qnum FROM quotes where author_id = a.id group by author_id) as num FROM authors a left join achivments ach on a.id =  ach.author_id where type = "' + type +'"';
	} else {
		sql ='SELECT a.id, a.name, a.description, a.icon, a.type, ach.name as ach_name, ach.img,if((SELECT count(id) as qnum FROM quotes where author_id = a.id group by author_id)=(select max(qnum) from (SELECT count(id) as qnum,author_id FROM quotes group by author_id) as tbl),1,NULL) as lead,(SELECT count(id) as qnum FROM quotes where author_id = a.id group by author_id) as num FROM authors a left join achivments ach on a.id =  ach.author_id order by a.type, a.name';
	}

console.log(sql);

pool.query(sql, function(err, rows, fields) {
		if (err) {
			res.status(500).json({"status_code": 500,"status_message": "internal server error"});
		} else {
			for (var i = 0; i < rows.length; i++) {

				var person = {
					'name':rows[i].name,
					'icon':rows[i].icon,
					'type':rows[i].type,
					'id':rows[i].id,
					'description':rows[i].description,
					'lead':rows[i].lead,
					'num':rows[i].num,
					'ach_name':rows[i].ach_name,
					'img':rows[i].img
				}
				usersList.push(person);
		}

		var unique = {};
		var distinct = [];
		    for( var i in usersList ){
		     if( typeof(unique[usersList[i].type]) == "undefined"){
		      distinct.push({'type':usersList[i].type});		      
		     }
		     unique[usersList[i].type] = 0;
		    }
		res.render('users', {"usersList": usersList , "typesList": distinct, title: 'Users'});
		}
	});
	
});


module.exports = router;
