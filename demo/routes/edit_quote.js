var express = require('express');
var mysql = require('mysql');
var async = require('async');
var router = express.Router();
var dateFormat = require('dateformat');


/* GET home page. */
router.get('/', function(req, res,next) {

	if(!req.cookies.jiracook){
		return res.render('login');
	}
	
	var quotesList = [];	
	var authorsList = [];
	var categoriesList = [];
	var typesList = [];
	var id = req.query.id;
	var author = req.query.author;

	var pool = mysql.createPool({
	connectionLimit : 2,
    host: 'localhost',    
    port: 3307,
    user: 'root',
    password: 'aZx112',
    database: 'jirateam'
});

	
	sql = 'SELECT q.id, q.text, q.img, q.created, q.description, a.name as author, q.author_id,a.icon, q.category_id, c.name as category, c.img as category_img, t.id as typeid, t.name as type, t.img as typeimg  FROM quotes q left join authors a on q.author_id=a.id left join categories c on q.category_id=c.id left join types t on q.type_id=t.id where q.id = ' + id ;	

	sqlAuthors = 'SELECT * from authors order by name ';

	sqlTypes = 'SELECT * from types order by name ';

	sqlCategories = 'SELECT * from categories order by name ';

console.log(sql);
async.parallel([
       function(parallel_done) {
			pool.query(sql, function(err, rows, fields) {
					if (err) {
						res.status(500).json({"status_code": 500,"status_message": "internal server error"});
						parallel_done(err);
					} else {
						for (var i = 0; i < rows.length; i++) {

							var quotes = {
								'id':rows[i].id,
								'text':rows[i].text,
								'img':rows[i].img,
								'created':dateFormat(rows[i].created,"yyyy-mm-dd"),
								'description':rows[i].description,					
								'author_id':rows[i].author_id,					
								'category_id':rows[i].category_id,					
								'type_id':rows[i].type_id
							}				
							quotesList.push(quotes);
					}

					console.log(quotesList.length);
					parallel_done();
					//res.render('adm_quotes', {"quotesList": quotesList});
					}
				});
		},
		function(parallel_done) {
			pool.query(sqlCategories, function(err, rows, fields) {
				console.log(sqlCategories);
				if (err) {
					res.status(500).json({"status_code": 500,"status_message": "internal server error"});
					parallel_done(err);
				} else {
						var categorie_null = {
							'id':0,
							'name':'no data'
						}				
						categoriesList.push(categorie_null);

						for (var i = 0; i < rows.length; i++) {
						var categories = {
							'id':rows[i].id,
							'name':rows[i].name
						}				
						categoriesList.push(categories);
				}

				parallel_done();
				}
			});
		},
		function(parallel_done) {
			pool.query(sqlAuthors, function(err, rows, fields) {
				console.log(sqlAuthors);
				if (err) {
					res.status(500).json({"status_code": 500,"status_message": "internal server error"});
					parallel_done(err);
				} else {
						var author_null = {
							'id':0,
							'name':'no data'
						}				
						authorsList.push(author_null);
						
						for (var i = 0; i < rows.length; i++) {
						var authors = {
							'id':rows[i].id,
							'name':rows[i].name
						}				
						authorsList.push(authors);
				}
				parallel_done();
				}
			});
		}		,
		function(parallel_done) {
			pool.query(sqlTypes, function(err, rows, fields) {
				console.log(sqlTypes);
				if (err) {
					res.status(500).json({"status_code": 500,"status_message": "internal server error"});
					parallel_done(err);
				} else {
						var type_null = {
							'id':0,
							'name':'no data'
						}				
						typesList.push(type_null);
					
					for (var i = 0; i < rows.length; i++) {
						var types = {
							'id':rows[i].id,
							'name':rows[i].name
						}				
						typesList.push(types);
					}

					parallel_done();
				}
			});
		}
	],
	function(err) {
         if (err) console.log(err);
         pool.end();
         res.render('edit_quote', {"quotesList": quotesList, "categoriesList": categoriesList, "authorsList": authorsList, "typesList": typesList, title: 'Edit Quote'});
    });



});

router.post('/', function(req, res) {

		if(!req.cookies.jiracook){
			return res.render('login');
		}		
		
		var id = req.query.id;

		var pool = mysql.createPool({
		connectionLimit : 2,
	    host: 'localhost',    
	    port: 3307,
	    user: 'root',
	    password: 'aZx112',
	    database: 'jirateam'
	});
		if(req.body.created && req.body.author_id && req.body.category_id && req.body.type_id) {

		// if(!req.body.description || req.body.description == '') {			
		// 	var description = "";
		// }

		var sql = "update quotes set text = '" + req.body.text +"', img = '" + req.body.img + "', created = '" + req.body.created + "', description = '" 
		+ req.body.description + "', author_id = " + req.body.author_id + ",category_id =  " + req.body.category_id + ", type_id = " + req.body.type_id  + " where id = " + req.body.id;
		
		console.log(sql);

		pool.query(sql, function(err, rows, fields) {
				if (err) {
					res.status(500).json({"status_code": 500,"status_message": "internal server error"});					
				} else {
					res.redirect('/adm_quotes');				
				}
			});

		} else {
			res.redirect('/adm_quotes?id=0');
		}

});

module.exports = router;