var express = require('express');
var router = express.Router();

var cookieParser = require('cookie-parser');

var app = express();
app.use(cookieParser());


/* GET /ea */
router.get('/', function(req, res, next) {
	
	if(!req.cookies.jiracook){
		return res.redirect('/login');
	} else {
  		res.render('ea', { title: 'EA'});
  	}
});


module.exports = router;
