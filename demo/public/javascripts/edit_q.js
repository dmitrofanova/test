$(document).ready( function() {
	var urlParams = new URLSearchParams(window.location.search);
	$("#author_id").val(urlParams.get('author'));
	$("#category_id").val(urlParams.get('category'));
	$("#type_id").val(urlParams.get('type'));
	$("#text").val($("#txt").val());
});